package inf319;

public class ProgressaoGeometrica extends Progressao {

	private int base;

	public ProgressaoGeometrica() {
		this(1, 2);
	}

	public ProgressaoGeometrica(int valorInicial) {
		this(valorInicial, 2);
	}
	
	public ProgressaoGeometrica(int valorInicial, int base) {
        inicia(valorInicial, base);
    }

	@Override
	public long inicia() {
		return inicia(valIni, base);
	}

	@Override
	public long inicia(int valorInicial) {
		inicia(valorInicial, 2);
		
		return valCor;
	}

	@Override
	public long inicia(int valorInicial, int base) {
		this.valCor = valorInicial;
    	this.valIni = valorInicial;
    	this.base = base;
    	
		return valCor;
	}

	@Override
	public long proxTermo() {
		valCor *= base;
		return valCor;
	}

}
