package inf319;

public class ProgressaoFibonacci extends Progressao {

	private int valPrev;
	private int valPrevI;

	public ProgressaoFibonacci() {
		this(0, 1);
	}
	
	public ProgressaoFibonacci(int valCor) {
		this(valCor, 1);
	}
	
	public ProgressaoFibonacci(int valCor, int valPrev) {
        inicia(valCor, valPrev);
    }

	@Override
	public long inicia() {
		return inicia(valIni, valPrevI);
	}

	@Override
	public long inicia(int valCor) {
		inicia(valCor, valPrevI);
		
		return valCor;
	}

	@Override
	public long inicia(int valCor, int valPrev) {
		this.valCor = valCor;
		this.valIni = valCor;
		this.valPrev = valPrev;
		this.valPrevI = valPrev;
		
		return valCor;
	}

	@Override
	public long proxTermo() {
		valCor += valPrev;
		valPrev = valCor - valPrev;
		return valCor;
	}
}
