package inf319;

public class ProgressaoAritmetica extends Progressao {
    
    private int incremento;

    public ProgressaoAritmetica() {
        this(0, 1);
    }

    public ProgressaoAritmetica(int valorInicial) {
        this(valorInicial, 1);
    }
    
    public ProgressaoAritmetica(int valorInicial, int incremento) {
        inicia(valorInicial, incremento);
    }

    @Override
    public long inicia() {
        return inicia(valIni, incremento);
    }
    
    @Override
    public long inicia(int valorInicial) {
    	inicia(valorInicial, incremento);
    	
        return valCor;
    }
    
    @Override
    public long inicia(int valorInicial, int incremento) {
    	this.valCor = valorInicial;
    	this.valIni = valorInicial;
    	this.incremento = incremento;
    	
        return valCor;
    }
    
    @Override
    public long proxTermo() {
        valCor += incremento;
        return valCor;
    }

}
