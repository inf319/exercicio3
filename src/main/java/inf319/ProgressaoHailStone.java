package inf319;

/**
 * Classe para calcular a progressão de Hailstone.
 * A classe herda de Progressao e implementa os métodos abstratos.
 * Nota-se que não houve nenhuma mudança fundamental no projeto original, somente foi adicionado 
 * esta classe com seu respectivo cálculo.
 * 
 * @author esoft17 - Guilherme Kayo Shida 
 *
 */
public class ProgressaoHailStone extends Progressao {
	
	private int valConvergente;
	
	public ProgressaoHailStone() {
        this(1, 1);
    }

    public ProgressaoHailStone(int valorInicial) {
        this(valorInicial, 1);
    }
    
    public ProgressaoHailStone(int valorInicial, int valorConvergente) {
        inicia(valorInicial, valorConvergente);
    }

	@Override
	public long inicia() {
		return inicia(valIni, valConvergente);
	}
	
	@Override
	public long inicia(int valorInicial) {
		inicia(valorInicial, valConvergente);
		
		return valCor;
	}

	@Override
	public long inicia(int valorInicial, int valorConvergente) {
		this.valCor = valorInicial;
		this.valIni = valorInicial;
		this.valConvergente = valorConvergente;
		
		return valCor;
	}

	@Override
	public long proxTermo() {
		if (valCor == valConvergente) {
			return valCor;
		}
		
		if (valCor % 2 == 0) {
			valCor = valCor / 2;
		} else {
			valCor = 3 * (valCor) + 1;
		}
		
		return valCor;
	}
}
